#Application to parse csv and generate reports by criteria

###About
$ROOT/backend   --> https://projects.spring.io/spring-boot/

$ROOT/frontend  --> https://angular.io/

csv parse lib   --> https://github.com/uniVocity/univocity-parsers

###How to build
mvn clean install

cd backend/target

rename backend-$Version.war to reporter.war

deploy reporter.war to servlet container (tomcat, Jboss etc)


###Serialize exception?
Just remove 2 file com.reporter.core.model.* from $YOUR_SERVLET_CONTAINER/bin/com.reporter.core.model*
