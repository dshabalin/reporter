import {Component, OnInit} from "@angular/core";
import {ReportService} from "../../service/report.service";
import {ModelObject} from "../../model/model-object";
import {SelectItem, Message} from "primeng/components/common/api";
import {UTILS} from "../Constants";
import {strictEqual} from "assert";

@Component({
  selector: 'app-csv-app',
  templateUrl: 'report-app.component.html',
  providers: [ReportService]
})
export class ReportAppComponent implements OnInit {
  private filtersSelected: string[] = [];
  private filterOptions: SelectItem[] = [];
  private templates: Template[] = [];
  private templateSelected: Template;

  private templateOptions: SelectItem[];
  private data: ModelObject[] = [];
  private rawData: ModelObject[] = [];

  //labels selected
  private selectedColumns: string[] = [];

  //current filter
  private criteria: any = {};

  private dataSources: SelectItem[];

  private dataSourceSelected: string;

  private errorMessage: any;

  constructor(private service: ReportService) {
  }

  /**
   * refresh this.criteria to delete NON showed filters if filter item
   * selected AND deselected from columns list
   * @param $event
   */
  refreshCriteria($event): void {
    let newCriteria = {};
    for (let column of this.selectedColumns) {
      newCriteria[column] = this.criteria[column];
    }
    this.criteria = newCriteria
  }

  toFilterTable(event: any, field: string) {
    let term = this.lower(this.criteria[field]);
    if (term == "") {
      this.data = this.rawData;
      return;
    }
    if (UTILS.STRING_FIELDS.indexOf(field) != -1) {

      this.filterStringField(term, field);

    } else if (UTILS.NUMBER_FIELDS.indexOf(field) != -1) {

      this.filterStringField(term, field);

    } else if (UTILS.DATE_FIELDS.indexOf(field) != -1) {

      this.filterDateField(term, field);

    } else if (UTILS.BOOLEAN_FIELDS.indexOf(field) != -1) {

      this.filterBooleanField(term, field);
    }

  }

  private getStringTerms(term: string, delitter?: string) {
    delitter = delitter || ",";
    let values = term.split(delitter);

    if (values.length == 0) {
      this.data = this.rawData;
      return null;
    }

    //check range
    //if (delitter == '-' && values.length != 2) return null;
    return values;
  }

  private filterDateField(term: string, field: string) {
    let delitter = '-';
    let values: string[] = this.getStringTerms(term, delitter);
    if (values == null || values.length > 2) {
      this.data = this.rawData;
      return;
    }
    let dateValues: Date[] = [];

    let newData: ModelObject[] = [];

    for (let s of values) {
      let day_month_year = s.split(".");
      if (day_month_year.length != 3) {
        this.data = this.rawData;
        return;
      }
      if (this.isDateStringConsistent(day_month_year)) {
        this.data = this.rawData;
        return;
      }
      //month from 0 to 11
      let millis = new Date(Date.UTC(parseInt(day_month_year[2]) + 2000, parseInt(day_month_year[1]) - 1, parseInt(day_month_year[0])));
      let date = new Date(millis.getTime() + millis.getTimezoneOffset() * 60 * 1000);
      dateValues.push(date);
    }

    if (dateValues.length == 2) {
      for (let i = 0; i < this.rawData.length; i++) {
        let rowDate = new Date(this.rawData[i][field]);
        if (rowDate.getTime() >= dateValues[0].getTime()
          && rowDate.getTime() <= dateValues[1].getTime()
        ) {
          newData.push(this.rawData[i])
        }
      }
    } else {
      for (let i = 0; i < this.rawData.length; i++) {
        let rowDate = new Date(this.rawData[i][field]);

        if (rowDate.getTime() == dateValues[0].getTime()) {
          newData.push(this.rawData[i])
        }

      }
    }


    this.data = newData;
  }

  private isDateStringConsistent(day_month_year: string[]) {
    return day_month_year[2].length != 2 || day_month_year[1].length != 2 || day_month_year[0].length != 2;
  }

  private filterStringField(term: string, field: string, delitter?: string) {
    let values: string[] = this.getStringTerms(term, delitter);
    if (values == null) {
      return;
    }

    let newData: ModelObject[] = [];

    for (let value of values) {

      for (let i = 0; i < this.rawData.length; i++) {

        if (this.lower(this.rawData[i][field]).indexOf(this.lower(value)) != -1) {
          newData.push(this.rawData[i])
        }

      }

    }

    this.data = newData;
  }

  private filterBooleanField(term: string, field: string, delitter?: string) {
    term = this.lower(term);
    if (term == "") {
      this.data = this.rawData;
      return;
    }
    let conversedTerm: boolean = false;
    switch (term) {
      case '1':
        conversedTerm = true;
        break;
      case '0':
        conversedTerm = false;
        break;
      default:
        this.data = this.rawData;
        return;
    }
    let newData: ModelObject[] = [];

    for (let i = 0; i < this.rawData.length; i++) {

      if (this.rawData[i][field] == conversedTerm) {
        newData.push(this.rawData[i])
      }
    }

    this.data = newData;
  }

  private lower(s: string) {
    return s == null ? '' : s.toLowerCase().trim();
  }

  ngOnInit() {
    this.selectedColumns = UTILS.DEFAULT_COLUMNS;
    this.filterOptions = this.generateSelectItems(UTILS.ALL_COLUMNS);
    this.getTemplates();
    this.getSourceList();
  }

  loadDataSource(event: any): void {
    this.service.loadCsv(this.dataSourceSelected, this.criteria)
      .subscribe(
        csvEntries => this.rawData = this.data = csvEntries,
        error => this.errorMessage = <any>error);
  }

  getTemplates(): void {
    this.service.getTemplates()
      .subscribe(
        templates => ( this.templates = templates,
          this.refreshTemplateOptions() )
        ,
        error => this.errorMessage = <any>error
      )
    ;
  }

  deleteTemplate(event:any): void {
      this.service.deleteTemplate(event.value.id)
        .subscribe(
          templates => ( this.templates = templates,
            this.refreshTemplateOptions() )
          ,
          error => this.errorMessage = <any>error
        )
      ;
    }

  getSourceList() {
    this.service.getCsvList()
      .subscribe(
        files => this.dataSources = this.generateSelectItems(files),
        error => this.errorMessage = <any>error);
  }

  saveTemplate() {
    this.service.saveTemplate(this.filtersSelected)
      .subscribe(
        templates => ( this.templates = templates,
          this.refreshTemplateOptions() )
        ,
        error => this.errorMessage = <any>error);
    this.getTemplates()
  }

  generateReport(event: any) {
    this.service.downloadReport(this.data, this.selectedColumns)
      .subscribe(
        data => this.downloadFile(data),
        error => this.errorMessage = <any>error);
  }

  downloadFile(data: Response) {
    var blob = new Blob([data], {type: 'text/csv'});

    var anchor = document.createElement("a");
    anchor.download = "report.csv";
    anchor.href = window.URL.createObjectURL(blob);
    anchor.click();
  }

  msgs: Message[];

  uploadedFiles: any[] = [];

  onUpload(event) {
    for (let file of event.files) {
      this.uploadedFiles.push(file);
    }
    this.getSourceList();

    this.msgs = [];
    this.msgs.push({severity: 'info', summary: 'File Uploaded', detail: ''});
  }

  private applyFilter() {
    this.filtersSelected = this.templateSelected.columns;
  }

  private applyDefaultFilters() {
    let dayOffset: number = 1800 * 24 * 60 * 60 * 1000;
    let millis = new Date();
    let toDate = new Date(millis.getTime() + millis.getTimezoneOffset() * 60 * 1000);
    let fromDate = new Date(toDate.getTime() - dayOffset);

    for (let i = 0; i < this.rawData.length; i++) {
      let rowDate = new Date(this.rawData[i]["ibmsOfferStartDate"]);
      let newData = [];
      if (rowDate != null && rowDate.getTime() >= fromDate.getTime() && rowDate.getTime() <= toDate.getTime()) {
        newData.push(this.rawData[i]);
      }
      this.data = newData;
    }


  }

  private getDefaultFilters() {
    return UTILS.defaultFilters;
  }

  private getDateFields(): string[] {
    return UTILS.DATE_FIELDS;
  }

  private getBooleanFields(): string[] {
    return UTILS.BOOLEAN_FIELDS;
  }

  private getAllColumns(): string[] {
    return UTILS.ALL_COLUMNS;
  }

  private getNumberFields(): string[] {
    return UTILS.NUMBER_FIELDS;
  }

  private getStringFields(): string[] {
    return UTILS.STRING_FIELDS;
  }

  public generateSelectItems(rawFields: string[]): SelectItem[] {
    let fields = [];
    for (let x of rawFields) {
      fields.push({label: x, value: x});
    }
    return fields;
  }

  public refreshTemplateOptions(): void {
    let name = function (columns: string[]) {
      let name = '';
      for (let i = 0; i < columns.length; i++) {
        name += columns[i];
        if (i != columns.length - 1) {
          name += ' | '
        }
      }
      return name;
    };

    let fields: SelectItem[] = [];
    for (let x of this.templates) {
      fields.push({label: name(x.columns), value: x});
    }
    this.templateOptions = fields;
  }
}

export interface Template {
  id: number;
  columns: string[];
}
