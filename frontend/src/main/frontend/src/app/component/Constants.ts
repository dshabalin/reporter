import {SelectItem} from "primeng/components/common/api";

export class UTILS{
  public static readonly STRING_FIELDS = ['ibmsGracenoteExemptStatus', 'GNStatus', 'gracenoteID', 'ciscoVAMStatus', 'ciscoVAMStatusMessage', 'senttoMilanoDate', 'skyStatusUpdate', 'acquisitions', 'channelCoordinators', 'epScheduling', 'ottMediaDelivery', 'ibmsQCPassedBy', 'MediaGCIngestStatus', 'ibmsDuration', 'mpxDuration', 'ibmsTXStatus', 'ibmsQCStatus', 'ibmsOfferStartTime', 'ibmsOfferEndTime', 'mpxOfferStartTime', 'mpxOfferEndTime', 'mediaID', 'titleID', 'listingID', 'Certification', 'ibmsOfferStatus', 'mpxProductAdded', 'mpxProductUpdated', 'mpxProductApproved', 'mpxProductState', 'mpxProductOfferStatus', 'productSubscriptions', 'ibmsSubscriptionCode', 'mpxSubscription', 'ibmsOfferHidden', 'ibmsArchive', 'mpxProgrammeName', 'ibmsSeriesName', 'ibmsProgrammeName', 'platform', 'vodPlatformName', 'dealCode', 'masterDealCode', 'ibmsControllingChannel', 'affiliate', 'dealType'];
  public static readonly DATE_FIELDS = ['lpsd', 'lped', 'ibmsOfferStartDate', 'ibmsOfferEndDate', 'mpxOfferStartDate', 'mpxOfferEndDate', 'mpxMediaAdded', 'mpxMediaUpdated', 'ibmsQCStatusUpdated', 'mpxProgramAdded', 'mpxProgramUpdated'];
  public static readonly NUMBER_FIELDS = ['ibmsYear', 'ibmsSeasonNumber', 'mpxSeasonNumber', 'ibmsEpisodeNo', 'mpxEpisodeNo'];
  public static readonly BOOLEAN_FIELDS = ['automatedCatchup', 'mpxGracenoteExemptStatus'];
  public static readonly DEFAULT_COLUMNS = ['platform', 'lpsd', 'lped', 'ibmsOfferStart', 'ibmsOfferEnd'];
  public static readonly ALL_COLUMNS = ['platform', 'vodPlatformName', 'dealCode', 'masterDealCode', 'ibmsControllingChannel', 'affiliate', 'automatedCatchup', 'dealType', 'ibmsYear', 'number', 'mpxProgrammeName', 'ibmsSeriesName', 'ibmsProgrammeName', 'ibmsSeasonNumber', 'mpxSeasonNumber', 'ibmsEpisodeNo', 'mpxEpisodeNo', 'mediaID', 'titleID', 'listingID', 'Certification', 'ibmsOfferStatus', 'mpxProductAdded', 'mpxProductUpd', 'mpxProductApproved', 'mpxProductState', 'mpxProductOfferStatus', 'productSubscriptions', 'ibmsSubscriptionCode', 'mpxSubscription', 'ibmsOfferHidden', 'ibmsArchive', 'lpsd', 'lped', 'ibmsOfferStart', 'ibmsOfferEnd', 'mpxOfferStart', 'mpxOfferEnd', 'ibmsOfferStartTime', 'ibmsOfferEndTime', 'mpxOfferStartTime', 'mpxOfferEndTime', 'mpxMediaAdded', 'mpxMediaUpd', 'MediaGCIngestStatus', 'ibmsDuration', 'mpxDuration', 'ibmsTXStatus', 'ibmsQCStatus', 'ibmsQCStatusUpd', 'ibmsQCPassedBy', 'mpxProgramAdded', 'mpxProgramUpd', 'mpxGracenoteExemptStatus', 'ibmsGracenoteExemptStatus', 'GNStatus', 'gracenoteID', 'ciscoVAMStatus', 'ciscoVAMStatusMessage', 'senttoMilano', 'skyStatusUp', 'acquisitions', 'channelCoordinators', 'epScheduling', 'ottMediaDelivery'];
  public static readonly defaultFilters: SelectItem[] = [{label: "ibmsOfferStartDate", value: "no older than 1800 previous day"}];



}
interface filter {
  name: string;
  columnFilters: ColumnFilter[];
}

interface ColumnFilter {
  columnName: string;
  columnValue: string;
}
