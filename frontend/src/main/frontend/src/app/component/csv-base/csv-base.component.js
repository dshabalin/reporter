"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var CsvBaseComponent = (function () {
    function CsvBaseComponent(service) {
        this.service = service;
        this.data = [];
        this.rawData = [];
    }
    CsvBaseComponent.prototype.ngOnInit = function () {
    };
    CsvBaseComponent.prototype.loadDataSource = function (event) {
        var _this = this;
        this.service.loadCsv(this.dataSourceSelected, this.criteria)
            .subscribe(function (csvEntries) { return _this.rawData = _this.data = csvEntries; }, function (error) { return _this.errorMessage = error; });
    };
    return CsvBaseComponent;
}());
CsvBaseComponent = __decorate([
    core_1.Component({
        selector: 'app-csv-base',
        templateUrl: './csv-base.component.html',
        styleUrls: ['./csv-base.component.css']
    })
], CsvBaseComponent);
exports.CsvBaseComponent = CsvBaseComponent;
