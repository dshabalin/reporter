import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {Response, Http, RequestOptions, Headers, URLSearchParams} from "@angular/http";

@Injectable()
export class BaseService {

  public  readonly BASE_API = '/reporter/api/';

  constructor(protected http: Http) {
  }

  protected get(methodApiPath: string, params?: URLSearchParams): Observable<Response> {
    if(params){
      let requestOptions = new RequestOptions();
      requestOptions.params = params;
      return this.http.get(this.BASE_API + methodApiPath, requestOptions)
    }
    return this.http.get(this.BASE_API + methodApiPath)
  }

  protected post(methodApiPath: string, params: any): Observable<Response> {
    return this.http.post(this.BASE_API + methodApiPath, params)
  }

  protected handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  protected extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

  protected extractBody(res: Response) {
    return res.arrayBuffer() || {};
  }

}
