import { Injectable } from '@angular/core';
import {Http, URLSearchParams} from "@angular/http";
import {Observable} from "rxjs";
import {BaseService} from "./base.service";
import {ModelObject} from "../model/model-object";
import {SelectItem} from "primeng/components/common/api";
import {Template} from "../component/csv-app/report-app.component";

@Injectable()
export class ReportService extends BaseService{

  constructor(protected http: Http){
    super(http)
  }


  loadCsv(csvName: string, filters: SelectItem[]): Observable<ModelObject[]>{

    return this.post('csv/load', {filters: filters, filename: csvName})
      .map(this.extractData)
      .catch(this.handleError);

  }

  getCsvList(): Observable<string[]>{
    return this.get('csv/')
      .map(this.extractData)
      .catch(this.handleError);
  }


  getTemplates(): Observable<Template[]>{
    return this.get('template/')
      .map(this.extractData)
      .catch(this.handleError);
  }

  deleteTemplate(id: any): Observable<Template[]>{
    let params: URLSearchParams = new URLSearchParams();
    params.set('templateId', id);

    return this.get('template/delete', params)
      .map(this.extractData)
      .catch(this.handleError);
  }

  saveTemplate(columns: string[]) {
    return this.post('template/save', columns)
      .map(this.extractData)
      .catch(this.handleError);
  }

  downloadReport(objects: ModelObject[], selectedColumns: string[]) {
    return this.post('csv/download', {objects, selectedColumns})
      .map(this.extractBody)
      .catch(this.handleError);
  }
}
