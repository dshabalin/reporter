import {BrowserModule , } from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from "@angular/router";
import {DataTableModule, MultiSelectModule, InputTextModule, DropdownModule ,
  ButtonModule, FileUploadModule, ListboxModule, FieldsetModule, ToolbarModule} from 'primeng/primeng';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import {AppComponent} from './app.component';
import {ReportAppComponent} from './component/csv-app/report-app.component';
import { DataTableComponent } from './component/data-table/data-table.component';

@NgModule({
  declarations: [
    AppComponent,
    ReportAppComponent,
    DataTableComponent
  ],
  imports: [
    RouterModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    DataTableModule,
    MultiSelectModule,
    InputTextModule,
    DropdownModule,
    BrowserAnimationsModule,
    ButtonModule,
    FileUploadModule,
    ListboxModule,
    FieldsetModule,
    ToolbarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
