package com.reporter.core.model;

import com.reporter.core.util.MyDateConversion;
import com.univocity.parsers.annotations.Convert;
import com.univocity.parsers.annotations.Parsed;
import com.univocity.parsers.annotations.Trim;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Date;

/**
 * Created by Дмитрий on 29.03.2017
 */

public class ModelObject implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final String[] ALLOWED_DATE_FORMATS = {"dd.MM.yyyy", "dd.MM.yy", "dd/MM/yyyy", "dd/MM/yy", "dd-MM-yy'T'hh:mm:ss", "yyyy-MM-dd'T'hh:mm:ss"};

    @Trim
    @Parsed(index = 0)
    public String platform;
    @Parsed(index = 1)
    public String vodPlatformName;
    @Parsed(index = 2)
    public String dealCode;
    @Parsed(index = 3)
    public String masterDealCode;
    @Parsed(index = 4)
    public String ibmsControllingChannel;
    @Parsed(index = 5)
    public String affiliate;
    @Parsed(index = 6)
    public Boolean automatedCatchup;
    @Parsed(index = 7)
    public String dealType;
    @Parsed(index = 8)
    public Integer ibmsYear;
    @Parsed(index = 9)
    public String mpxProgrammeName;
    @Parsed(index = 10)
    public String ibmsSeriesName;
    @Parsed(index = 11)
    public String ibmsProgrammeName;
    @Parsed(index = 12)
    public Integer ibmsSeasonNumber;
    @Parsed(index = 13)
    public Integer mpxSeasonNumber;
    @Parsed(index = 14)
    public Integer ibmsEpisodeNo;
    @Parsed(index = 15)
    public Integer mpxEpisodeNo;
    @Parsed(index = 16)
    public String mediaID;
    @Parsed(index = 17)
    public String titleID;
    @Parsed(index = 18)
    public String listingID;
    @Parsed(index = 19)
    public String Certification;
    @Parsed(index = 20)
    public String ibmsOfferStatus;
    @Parsed(index = 21)
    public String mpxProductAdded;
    @Parsed(index = 22)
    public String mpxProductUpdated;
    @Parsed(index = 23)
    public String mpxProductApproved;
    @Parsed(index = 24)
    public String mpxProductState;
    @Parsed(index = 25)
    public String mpxProductOfferStatus;
    @Parsed(index = 26)
    public String productSubscriptions;
    @Parsed(index = 27)
    public String ibmsSubscriptionCode;
    @Parsed(index = 28)
    public String mpxSubscription;
    @Parsed(index = 29)
    public String ibmsOfferHidden;
    @Parsed(index = 30)
    public String ibmsArchive;
    @Convert(conversionClass = MyDateConversion.class, args = {"dd.MM.yyyy", "dd.MM.yy", "dd/MM/yyyy", "dd/MM/yy", "dd-MM-yy'T'hh:mm:ss", "yyyy-MM-dd'T'hh:mm:ss"})
    @Parsed(index = 31)
    public Date lpsd;
    @Parsed(index = 32)
    @Convert(conversionClass = MyDateConversion.class,
            args = {"dd.MM.yyyy", "dd.MM.yy", "dd/MM/yyyy", "dd/MM/yy", "dd-MM-yy'T'hh:mm:ss", "yyyy-MM-dd'T'hh:mm:ss"})
    public Date lped;

    @Parsed(index = 33)
    @Convert(conversionClass = MyDateConversion.class,
            args = {"dd.MM.yyyy", "dd.MM.yy", "dd/MM/yyyy", "dd/MM/yy", "dd-MM-yy'T'hh:mm:ss", "yyyy-MM-dd'T'hh:mm:ss"})
    public Date ibmsOfferStartDate;

    @Parsed(index = 34)
    @Convert(conversionClass = MyDateConversion.class,
            args = {"dd.MM.yyyy", "dd.MM.yy", "dd/MM/yyyy", "dd/MM/yy", "dd-MM-yy'T'hh:mm:ss", "yyyy-MM-dd'T'hh:mm:ss"})
    public Date ibmsOfferEndDate;

    @Parsed(index = 35)
    @Convert(conversionClass = MyDateConversion.class,
            args = {"dd.MM.yyyy", "dd.MM.yy", "dd/MM/yyyy", "dd/MM/yy", "dd-MM-yy'T'hh:mm:ss", "yyyy-MM-dd'T'hh:mm:ss"})
    public Date mpxOfferStartDate;

    @Parsed(index = 36)
    @Convert(conversionClass = MyDateConversion.class,
            args = {"dd.MM.yyyy", "dd.MM.yy", "dd/MM/yyyy", "dd/MM/yy", "dd-MM-yy'T'hh:mm:ss", "yyyy-MM-dd'T'hh:mm:ss"})
    public Date mpxOfferEndDate;

    @Parsed(index = 37)
    //TODO: time
    public String ibmsOfferStartTime;
    @Parsed(index = 38)
    //TODO: time
    public String ibmsOfferEndTime;
    @Parsed(index = 39)
    public String mpxOfferStartTime;
    @Parsed(index = 40)
    public String mpxOfferEndTime;

    @Parsed(index = 41)
    @Convert(conversionClass = MyDateConversion.class,
            args = {"dd.MM.yyyy", "dd.MM.yy", "dd/MM/yyyy", "dd/MM/yy", "dd-MM-yy'T'hh:mm:ss", "yyyy-MM-dd'T'hh:mm:ss"})
    public Date mpxMediaAdded;

    @Parsed(index = 42)
    @Convert(conversionClass = MyDateConversion.class,
            args = {"dd.MM.yyyy", "dd.MM.yy", "dd/MM/yyyy", "dd/MM/yy", "dd-MM-yy'T'hh:mm:ss", "yyyy-MM-dd'T'hh:mm:ss"})
    public Date mpxMediaUpdated;
    @Parsed(index = 43)
    public String MediaGCIngestStatus;
    @Parsed(index = 44)
    public String ibmsDuration;
    @Parsed(index = 45)
    public String mpxDuration;
    @Parsed(index = 46)
    public String ibmsTXStatus;
    @Parsed(index = 47)
    public String ibmsQCStatus;

    @Parsed(index = 48)
    @Convert(conversionClass = MyDateConversion.class,
            args = {"dd.MM.yyyy", "dd.MM.yy", "dd/MM/yyyy", "dd/MM/yy", "dd-MM-yy'T'hh:mm:ss", "yyyy-MM-dd'T'hh:mm:ss"})
    public Date ibmsQCStatusUpdated;

    @Parsed(index = 49)
    public String ibmsQCPassedBy;

    @Parsed(index = 50)
    @Convert(conversionClass = MyDateConversion.class,
            args = {"dd.MM.yyyy", "dd.MM.yy", "dd/MM/yyyy", "dd/MM/yy", "dd-MM-yy'T'hh:mm:ss", "yyyy-MM-dd'T'hh:mm:ss"})
    public Date mpxProgramAdded;

    @Parsed(index = 51)
    @Convert(conversionClass = MyDateConversion.class,
            args = {"dd.MM.yyyy", "dd.MM.yy", "dd/MM/yyyy", "dd/MM/yy", "dd-MM-yy'T'hh:mm:ss", "yyyy-MM-dd'T'hh:mm:ss"})
    public Date mpxProgramUpdated;

    @Parsed(index = 52)
    public Boolean mpxGracenoteExemptStatus;
    @Parsed(index = 53)
    public String ibmsGracenoteExemptStatus;
    @Parsed(index = 54)
    public String GNStatus;
    @Parsed(index = 55)
    public String gracenoteID;
    @Parsed(index = 56)
    public String ciscoVAMStatus;
    @Parsed(index = 57)
    public String ciscoVAMStatusMessage;
    @Parsed(index = 58)
    public String senttoMilanoDate;
    @Parsed(index = 59)
    public String skyStatusUpdate;
    @Parsed(index = 60)
    public String acquisitions;
    @Parsed(index = 61)
    public String channelCoordinators;
    @Parsed(index = 62)
    public String epScheduling;
    @Parsed(index = 63)
    public String ottMediaDelivery;


    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getVodPlatformName() {
        return vodPlatformName;
    }

    public void setVodPlatformName(String vodPlatformName) {
        this.vodPlatformName = vodPlatformName;
    }

    public String getDealCode() {
        return dealCode;
    }

    public void setDealCode(String dealCode) {
        this.dealCode = dealCode;
    }

    public String getMasterDealCode() {
        return masterDealCode;
    }

    public void setMasterDealCode(String masterDealCode) {
        this.masterDealCode = masterDealCode;
    }

    public String getIbmsControllingChannel() {
        return ibmsControllingChannel;
    }

    public void setIbmsControllingChannel(String ibmsControllingChannel) {
        this.ibmsControllingChannel = ibmsControllingChannel;
    }

    public String getAffiliate() {
        return affiliate;
    }

    public void setAffiliate(String affiliate) {
        this.affiliate = affiliate;
    }

    public Boolean getAutomatedCatchup() {
        return automatedCatchup;
    }

    public void setAutomatedCatchup(Boolean automatedCatchup) {
        this.automatedCatchup = automatedCatchup;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public Integer getIbmsYear() {
        return ibmsYear;
    }

    public void setIbmsYear(Integer ibmsYear) {
        this.ibmsYear = ibmsYear;
    }

    public String getMpxProgrammeName() {
        return mpxProgrammeName;
    }

    public void setMpxProgrammeName(String mpxProgrammeName) {
        this.mpxProgrammeName = mpxProgrammeName;
    }

    public String getIbmsSeriesName() {
        return ibmsSeriesName;
    }

    public void setIbmsSeriesName(String ibmsSeriesName) {
        this.ibmsSeriesName = ibmsSeriesName;
    }

    public String getIbmsProgrammeName() {
        return ibmsProgrammeName;
    }

    public void setIbmsProgrammeName(String ibmsProgrammeName) {
        this.ibmsProgrammeName = ibmsProgrammeName;
    }

    public Integer getIbmsSeasonNumber() {
        return ibmsSeasonNumber;
    }

    public void setIbmsSeasonNumber(Integer ibmsSeasonNumber) {
        this.ibmsSeasonNumber = ibmsSeasonNumber;
    }

    public Integer getMpxSeasonNumber() {
        return mpxSeasonNumber;
    }

    public void setMpxSeasonNumber(Integer mpxSeasonNumber) {
        this.mpxSeasonNumber = mpxSeasonNumber;
    }

    public Integer getIbmsEpisodeNo() {
        return ibmsEpisodeNo;
    }

    public void setIbmsEpisodeNo(Integer ibmsEpisodeNo) {
        this.ibmsEpisodeNo = ibmsEpisodeNo;
    }

    public Integer getMpxEpisodeNo() {
        return mpxEpisodeNo;
    }

    public void setMpxEpisodeNo(Integer mpxEpisodeNo) {
        this.mpxEpisodeNo = mpxEpisodeNo;
    }

    public String getMediaID() {
        return mediaID;
    }

    public void setMediaID(String mediaID) {
        this.mediaID = mediaID;
    }

    public String getTitleID() {
        return titleID;
    }

    public void setTitleID(String titleID) {
        this.titleID = titleID;
    }

    public String getListingID() {
        return listingID;
    }

    public void setListingID(String listingID) {
        this.listingID = listingID;
    }

    public String getCertification() {
        return Certification;
    }

    public void setCertification(String certification) {
        Certification = certification;
    }

    public String getIbmsOfferStatus() {
        return ibmsOfferStatus;
    }

    public void setIbmsOfferStatus(String ibmsOfferStatus) {
        this.ibmsOfferStatus = ibmsOfferStatus;
    }

    public String getMpxProductAdded() {
        return mpxProductAdded;
    }

    public void setMpxProductAdded(String mpxProductAdded) {
        this.mpxProductAdded = mpxProductAdded;
    }

    public String getMpxProductUpdated() {
        return mpxProductUpdated;
    }

    public void setMpxProductUpdated(String mpxProductUpdated) {
        this.mpxProductUpdated = mpxProductUpdated;
    }

    public String getMpxProductApproved() {
        return mpxProductApproved;
    }

    public void setMpxProductApproved(String mpxProductApproved) {
        this.mpxProductApproved = mpxProductApproved;
    }

    public String getMpxProductState() {
        return mpxProductState;
    }

    public void setMpxProductState(String mpxProductState) {
        this.mpxProductState = mpxProductState;
    }

    public String getMpxProductOfferStatus() {
        return mpxProductOfferStatus;
    }

    public void setMpxProductOfferStatus(String mpxProductOfferStatus) {
        this.mpxProductOfferStatus = mpxProductOfferStatus;
    }

    public String getProductSubscriptions() {
        return productSubscriptions;
    }

    public void setProductSubscriptions(String productSubscriptions) {
        this.productSubscriptions = productSubscriptions;
    }

    public String getIbmsSubscriptionCode() {
        return ibmsSubscriptionCode;
    }

    public void setIbmsSubscriptionCode(String ibmsSubscriptionCode) {
        this.ibmsSubscriptionCode = ibmsSubscriptionCode;
    }

    public String getMpxSubscription() {
        return mpxSubscription;
    }

    public void setMpxSubscription(String mpxSubscription) {
        this.mpxSubscription = mpxSubscription;
    }

    public String getIbmsOfferHidden() {
        return ibmsOfferHidden;
    }

    public void setIbmsOfferHidden(String ibmsOfferHidden) {
        this.ibmsOfferHidden = ibmsOfferHidden;
    }

    public String getIbmsArchive() {
        return ibmsArchive;
    }

    public void setIbmsArchive(String ibmsArchive) {
        this.ibmsArchive = ibmsArchive;
    }

    public Date getLpsd() {
        return lpsd;
    }

    public void setLpsd(Date lpsd) {
        this.lpsd = lpsd;
    }

    public Date getLped() {
        return lped;
    }

    public void setLped(Date lped) {
        this.lped = lped;
    }

    public Date getIbmsOfferStartDate() {
        return ibmsOfferStartDate;
    }

    public void setIbmsOfferStartDate(Date ibmsOfferStartDate) {
        this.ibmsOfferStartDate = ibmsOfferStartDate;
    }

    public Date getIbmsOfferEndDate() {
        return ibmsOfferEndDate;
    }

    public void setIbmsOfferEndDate(Date ibmsOfferEndDate) {
        this.ibmsOfferEndDate = ibmsOfferEndDate;
    }

    public Date getMpxOfferStartDate() {
        return mpxOfferStartDate;
    }

    public void setMpxOfferStartDate(Date mpxOfferStartDate) {
        this.mpxOfferStartDate = mpxOfferStartDate;
    }

    public Date getMpxOfferEndDate() {
        return mpxOfferEndDate;
    }

    public void setMpxOfferEndDate(Date mpxOfferEndDate) {
        this.mpxOfferEndDate = mpxOfferEndDate;
    }

    public String getIbmsOfferStartTime() {
        return ibmsOfferStartTime;
    }

    public void setIbmsOfferStartTime(String ibmsOfferStartTime) {
        this.ibmsOfferStartTime = ibmsOfferStartTime;
    }

    public String getIbmsOfferEndTime() {
        return ibmsOfferEndTime;
    }

    public void setIbmsOfferEndTime(String ibmsOfferEndTime) {
        this.ibmsOfferEndTime = ibmsOfferEndTime;
    }

    public String getMpxOfferStartTime() {
        return mpxOfferStartTime;
    }

    public void setMpxOfferStartTime(String mpxOfferStartTime) {
        this.mpxOfferStartTime = mpxOfferStartTime;
    }

    public String getMpxOfferEndTime() {
        return mpxOfferEndTime;
    }

    public void setMpxOfferEndTime(String mpxOfferEndTime) {
        this.mpxOfferEndTime = mpxOfferEndTime;
    }

    public Date getMpxMediaAdded() {
        return mpxMediaAdded;
    }

    public void setMpxMediaAdded(Date mpxMediaAdded) {
        this.mpxMediaAdded = mpxMediaAdded;
    }

    public Date getMpxMediaUpdated() {
        return mpxMediaUpdated;
    }

    public void setMpxMediaUpdated(Date mpxMediaUpdated) {
        this.mpxMediaUpdated = mpxMediaUpdated;
    }

    public String getMediaGCIngestStatus() {
        return MediaGCIngestStatus;
    }

    public void setMediaGCIngestStatus(String mediaGCIngestStatus) {
        MediaGCIngestStatus = mediaGCIngestStatus;
    }

    public String getIbmsDuration() {
        return ibmsDuration;
    }

    public void setIbmsDuration(String ibmsDuration) {
        this.ibmsDuration = ibmsDuration;
    }

    public String getMpxDuration() {
        return mpxDuration;
    }

    public void setMpxDuration(String mpxDuration) {
        this.mpxDuration = mpxDuration;
    }

    public String getIbmsTXStatus() {
        return ibmsTXStatus;
    }

    public void setIbmsTXStatus(String ibmsTXStatus) {
        this.ibmsTXStatus = ibmsTXStatus;
    }

    public String getIbmsQCStatus() {
        return ibmsQCStatus;
    }

    public void setIbmsQCStatus(String ibmsQCStatus) {
        this.ibmsQCStatus = ibmsQCStatus;
    }

    public Date getIbmsQCStatusUpdated() {
        return ibmsQCStatusUpdated;
    }

    public void setIbmsQCStatusUpdated(Date ibmsQCStatusUpdated) {
        this.ibmsQCStatusUpdated = ibmsQCStatusUpdated;
    }

    public String getIbmsQCPassedBy() {
        return ibmsQCPassedBy;
    }

    public void setIbmsQCPassedBy(String ibmsQCPassedBy) {
        this.ibmsQCPassedBy = ibmsQCPassedBy;
    }

    public Date getMpxProgramAdded() {
        return mpxProgramAdded;
    }

    public void setMpxProgramAdded(Date mpxProgramAdded) {
        this.mpxProgramAdded = mpxProgramAdded;
    }

    public Date getMpxProgramUpdated() {
        return mpxProgramUpdated;
    }

    public void setMpxProgramUpdated(Date mpxProgramUpdated) {
        this.mpxProgramUpdated = mpxProgramUpdated;
    }

    public Boolean getMpxGracenoteExemptStatus() {
        return mpxGracenoteExemptStatus;
    }

    public void setMpxGracenoteExemptStatus(Boolean mpxGracenoteExemptStatus) {
        this.mpxGracenoteExemptStatus = mpxGracenoteExemptStatus;
    }

    public String getIbmsGracenoteExemptStatus() {
        return ibmsGracenoteExemptStatus;
    }

    public void setIbmsGracenoteExemptStatus(String ibmsGracenoteExemptStatus) {
        this.ibmsGracenoteExemptStatus = ibmsGracenoteExemptStatus;
    }

    public String getGNStatus() {
        return GNStatus;
    }

    public void setGNStatus(String GNStatus) {
        this.GNStatus = GNStatus;
    }

    public String getGracenoteID() {
        return gracenoteID;
    }

    public void setGracenoteID(String gracenoteID) {
        this.gracenoteID = gracenoteID;
    }

    public String getCiscoVAMStatus() {
        return ciscoVAMStatus;
    }

    public void setCiscoVAMStatus(String ciscoVAMStatus) {
        this.ciscoVAMStatus = ciscoVAMStatus;
    }

    public String getCiscoVAMStatusMessage() {
        return ciscoVAMStatusMessage;
    }

    public void setCiscoVAMStatusMessage(String ciscoVAMStatusMessage) {
        this.ciscoVAMStatusMessage = ciscoVAMStatusMessage;
    }

    public String getSenttoMilanoDate() {
        return senttoMilanoDate;
    }

    public void setSenttoMilanoDate(String senttoMilanoDate) {
        this.senttoMilanoDate = senttoMilanoDate;
    }

    public String getSkyStatusUpdate() {
        return skyStatusUpdate;
    }

    public void setSkyStatusUpdate(String skyStatusUpdate) {
        this.skyStatusUpdate = skyStatusUpdate;
    }

    public String getAcquisitions() {
        return acquisitions;
    }

    public void setAcquisitions(String acquisitions) {
        this.acquisitions = acquisitions;
    }

    public String getChannelCoordinators() {
        return channelCoordinators;
    }

    public void setChannelCoordinators(String channelCoordinators) {
        this.channelCoordinators = channelCoordinators;
    }

    public String getEpScheduling() {
        return epScheduling;
    }

    public void setEpScheduling(String epScheduling) {
        this.epScheduling = epScheduling;
    }

    public String getOttMediaDelivery() {
        return ottMediaDelivery;
    }

    public void setOttMediaDelivery(String ottMediaDelivery) {
        this.ottMediaDelivery = ottMediaDelivery;
    }

    public boolean isAllFieldsNull() {
        ModelObject o = this;
        for (Field field : o.getClass().getDeclaredFields()) {
            //only for fields from CSV
            if (!field.isAnnotationPresent(Parsed.class)) {
                continue;
            }
            Class t = field.getType();
            Object value = null;
            try {
                value = field.get(o);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            if (value != null) return false;
        }
        return true;
    }
}
