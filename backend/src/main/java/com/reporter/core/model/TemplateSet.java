package com.reporter.core.model;

import java.io.*;
import java.util.*;

public class TemplateSet implements Serializable {
    private static final long serialVersionUID = 6529685098267757690L;


    private List<Template> templates;

    public static class Template implements Serializable{
        private static final long serialVersionUID = 6529685098267757690L;

        public String[] columns;
        public long id;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Template)) return false;
            Template template = (Template) o;
            if (!Arrays.equals(columns, template.columns)) return false;
            return id == template.id;
        }


        public Template(String[] columns) {
            this.id = new Date().getTime();
            this.columns = columns;

        }


    }


    public static void serialize(TemplateSet templateSet){
        try (ObjectOutputStream oos =
                     new ObjectOutputStream(new FileOutputStream(templateSet.getClass().getName()))) {
            oos.writeObject(templateSet);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    private static void serilializeNew(){
        try (ObjectOutputStream oos =
                     new ObjectOutputStream(new FileOutputStream(TemplateSet.class.getName()))) {
            TemplateSet templateSet = new TemplateSet();
            templateSet.setTemplates(new ArrayList<>());
            oos.writeObject(templateSet);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public static TemplateSet loadTemplateSet(){
        TemplateSet entity = null;
        String fileName = TemplateSet.class.getName();
        File file=new File(fileName);
        if(!file.exists())serilializeNew();
        try (ObjectInputStream ois
                     = new ObjectInputStream(new FileInputStream(fileName))) {

            entity = (TemplateSet) ois.readObject();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return entity;
    }

    public static List<Template> addTemplateAndSave(String[] columns) {
        if(columns != null && columns.length > 0){
            TemplateSet templateSet = loadTemplateSet();
            templateSet.templates.add(new Template(columns));
            serialize(templateSet);
            return templateSet.getTemplates();
        }
        throw new IllegalArgumentException(String.format("template %s is wrong", Arrays.toString(columns)));
    }

    public static List<Template> removeTemplateAndSave(long id) {
        TemplateSet templateSet = loadTemplateSet();
        int i = 0;
        for(Template t: templateSet.getTemplates()){
            if(t.id == id){
                templateSet.getTemplates().remove(i);
                serialize(templateSet);
                return templateSet.getTemplates();
            }
            i++;
        }
        return templateSet.getTemplates();
    }



    public List<Template> getTemplates() {
        return templates;
    }

    private void setTemplates(List<Template> templates) {
        this.templates = templates;
    }


}


