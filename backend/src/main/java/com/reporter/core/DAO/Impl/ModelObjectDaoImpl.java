package com.reporter.core.DAO.Impl;

import com.reporter.Utils;
import com.reporter.core.DAO.ModelObjectDao;
import com.reporter.core.model.ModelObject;
import com.univocity.parsers.common.processor.BeanListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by Дмитрий on 29.03.2017
 */
public class ModelObjectDaoImpl implements ModelObjectDao <ModelObject> {
    BeanListProcessor<ModelObject> rowProcessor;

    public ModelObjectDaoImpl(String fileName) throws FileNotFoundException {
        // BeanListProcessor converts each parsed row to an instance of a given class, then stores each instance into a list.
        rowProcessor = new BeanListProcessor<ModelObject>(ModelObject.class);
        CsvParserSettings parserSettings = new CsvParserSettings();
        parserSettings.setRowProcessor(rowProcessor);
        parserSettings.setHeaderExtractionEnabled(true);

        CsvParser parser = new CsvParser(parserSettings);
        File file = new File("CSV_HOLDER" + File.separator + fileName);
        parser.parse(new FileReader(file));
    };

    public static void saveCsv(byte[] content, String filename) throws FileNotFoundException {
        File outFile = new File("CSV_HOLDER" + File.separator + filename);
        try {
            BufferedOutputStream stream = new BufferedOutputStream(
                    new FileOutputStream(outFile));
            stream.write(content);
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    };

    public List<ModelObject> find() {
        List<ModelObject> entries = rowProcessor.getBeans();
        entries = entries.stream().filter(p -> !p.isAllFieldsNull()).collect(Collectors.toList());
        return entries;
    }

    @Override
    public List<ModelObject> find(Predicate<ModelObject> predicate) {
        List<ModelObject> beans = find();
        return beans.stream().filter(predicate).collect(Collectors.toList());
    }
}
