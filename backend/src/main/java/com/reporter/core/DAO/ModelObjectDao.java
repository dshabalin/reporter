package com.reporter.core.DAO;

import java.util.List;
import java.util.function.Predicate;

/**
 * Created by Дмитрий on 29.03.2017
 */
public interface ModelObjectDao<T> {

    List<T> find();

    List<T> find(Predicate<T> predicate);
}
