package com.reporter.core.util;

import com.univocity.parsers.conversions.DateConversion;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by Дмитрий on 04.04.2017
 */
public class MyDateConversion extends DateConversion {

    public MyDateConversion(Date valueIfStringIsNull, String valueIfObjectIsNull, String... dateFormats) {
        super(valueIfStringIsNull, valueIfObjectIsNull, dateFormats);
    }

    public MyDateConversion(String... dateFormats) {
        super(dateFormats);
    }

    @Override
    protected Date fromString(String input) {
        for (SimpleDateFormat formatter : getFormatterObjects()) {
            try {
                return formatter.parse(input);
            } catch (ParseException ex) {
                System.out.println("Cannot parse '{value}' as a valid date. Supported formats are: " + Arrays.toString(getFormatterObjects()));

            }
        }
        return null;
    }
}
