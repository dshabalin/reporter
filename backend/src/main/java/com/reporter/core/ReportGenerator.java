package com.reporter.core;

import com.reporter.core.model.ModelObject;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Дмитрий on 01.04.2017
 */
public class ReportGenerator {

    public File generate(List<ModelObject> objects, String[] selectedColumns) {
        Configuration cfg = new Configuration();
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("report.ftl").getFile());

            Template template = new Template("report.ftl", new FileReader(file), cfg);

            Map<String, Object> data = new HashMap<String, Object>();

            List<String> STRING_FIELDS = Arrays.asList("ibmsGracenoteExemptStatus", "GNStatus", "gracenoteID", "ciscoVAMStatus", "ciscoVAMStatusMessage", "senttoMilanoDate", "skyStatusUpdate", "acquisitions", "channelCoordinators", "epScheduling", "ottMediaDelivery", "ibmsQCPassedBy", "MediaGCIngestStatus", "ibmsDuration", "mpxDuration", "ibmsTXStatus", "ibmsQCStatus", "ibmsOfferStartTime", "ibmsOfferEndTime", "mpxOfferStartTime", "mpxOfferEndTime", "mediaID", "titleID", "listingID", "Certification", "ibmsOfferStatus", "mpxProductAdded", "mpxProductUpdated", "mpxProductApproved", "mpxProductState", "mpxProductOfferStatus", "productSubscriptions", "ibmsSubscriptionCode", "mpxSubscription", "ibmsOfferHidden", "ibmsArchive", "mpxProgrammeName", "ibmsSeriesName", "ibmsProgrammeName", "platform", "vodPlatformName", "dealCode", "masterDealCode", "ibmsControllingChannel", "affiliate", "dealType");
            List<String> DATE_FIELDS = Arrays.asList("lpsd", "lped", "ibmsOfferStartDate", "ibmsOfferEndDate", "mpxOfferStartDate", "mpxOfferEndDate", "mpxMediaAdded", "mpxMediaUpdated", "ibmsQCStatusUpdated", "mpxProgramAdded", "mpxProgramUpdated");
            List<String> NUMBER_FIELDS = Arrays.asList("ibmsYear", "ibmsSeasonNumber", "mpxSeasonNumber", "ibmsEpisodeNo", "mpxEpisodeNo");
            List<String> BOOLEAN_FIELDS = Arrays.asList("automatedCatchup", "mpxGracenoteExemptStatus");

            data.put("modelObjects", objects);
            data.put("columns", selectedColumns);
            data.put("STRING_FIELDS", STRING_FIELDS);
            data.put("DATE_FIELDS", DATE_FIELDS);
            data.put("BOOLEAN_FIELDS", BOOLEAN_FIELDS);
            data.put("NUMBER_FIELDS", NUMBER_FIELDS);

            Writer out = new OutputStreamWriter(System.out);
            template.process(data, out);
            out.flush();

            return writeReport(template, data);


        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }


        return null;
    }

    private File writeReport(Template template, Map<String, Object> data) throws IOException, TemplateException {
        File outDirectory = new File("CSV_OUTPUT");
        if (!outDirectory.exists()) {
            outDirectory.mkdir();
        }
        File outFile = new File(createReportFilePath(outDirectory));
        Writer writer = new FileWriter(outFile);
        template.process(data, writer);
        writer.flush();
        writer.close();
        return outFile;
    }

    private String createReportFilePath(File directory) {
        DateFormat df = new SimpleDateFormat("MM_dd_yyyy_HH_mm_ss");
        String fileName = df.format(new Date());
        return directory.getAbsolutePath() + File.separator + fileName + ".csv";
    }
}
