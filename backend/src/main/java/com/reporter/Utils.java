package com.reporter;

import java.io.File;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Дмитрий on 01.04.2017
 */
public class Utils {

    public static File[] getCsvFiles(String path){
        File directory = new File(path);
        if(!directory.exists()){
            throw new IllegalStateException(String.format("directory {0} not exist", path));
        }
        return Arrays.stream(directory.listFiles())
                .filter(file -> (file != null && getFileExtension(file).equalsIgnoreCase("CSV"))).toArray(File[]::new);
    }

    private static String getFileExtension(File file) {
        String name = file.getName();
        try {
            return name.substring(name.lastIndexOf(".") + 1);
        } catch (Exception e) {
            return "";
        }
    }

}
