package com.reporter.web.wrappers;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Дмитрий on 03.04.2017
 */
public class FilterWrapper {
    public String filename;
    public Map<String, String> filters;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Map<String, String> getFilters() {
        return filters;
    }

    public void setFilters(Map<String, String> filters) {
        this.filters = filters;
    }
}
