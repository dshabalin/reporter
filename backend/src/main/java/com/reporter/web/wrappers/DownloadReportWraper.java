package com.reporter.web.wrappers;

import com.reporter.core.model.ModelObject;

import java.util.List;

/**
 * Created by Дмитрий on 04.04.2017
 */
public class DownloadReportWraper {
    String[] selectedColumns;
    List<ModelObject> objects;

    public String[] getSelectedColumns() {
        return selectedColumns;
    }

    public void setSelectedColumns(String[] selectedColumns) {
        this.selectedColumns = selectedColumns;
    }

    public List<ModelObject> getObjects() {
        return objects;
    }

    public void setObjects(List<ModelObject> objects) {
        this.objects = objects;
    }
}
