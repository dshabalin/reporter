package com.reporter.web;

import com.reporter.Utils;
import com.reporter.core.DAO.Impl.ModelObjectDaoImpl;
import com.reporter.core.DAO.ModelObjectDao;
import com.reporter.core.ReportGenerator;
import com.reporter.core.model.ModelObject;
import com.reporter.core.model.TemplateSet;
import com.reporter.web.wrappers.DownloadReportWraper;
import com.reporter.web.wrappers.FilterWrapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(applicationClass, args);

    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(applicationClass);
    }

    private static Class<Application> applicationClass = Application.class;

}
@RestController
class Controller {
    @RequestMapping(value = "/api/csv/load", method = RequestMethod.POST)
    List<ModelObject> loadCsv(@RequestBody FilterWrapper wrapper) throws IOException {
        List<ModelObject> list = new ArrayList<>();
        Assert.notNull(wrapper.filename);
        try {
            ModelObjectDao<ModelObject> dao = new ModelObjectDaoImpl(wrapper.filename);
            list = dao.find();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
        return list;
    }

    @RequestMapping(value = "/api/csv/upload", method = RequestMethod.POST)
    public @ResponseBody
    String uploadFileHandler(StandardMultipartHttpServletRequest request) {
        MultipartFile multipartFile = request.getFile("file");

        if (multipartFile!= null && StringUtils.isNotBlank(multipartFile.getOriginalFilename())) {
            try {
                ModelObjectDaoImpl.saveCsv(multipartFile.getBytes(), multipartFile.getOriginalFilename());
                return "Done";
            } catch (IOException e) {
                e.printStackTrace();
                return "File not found";
            }
        }
        return "Error";
    }

    @RequestMapping("/api/template")
    List<TemplateSet.Template> getTemplates() {
        List<TemplateSet.Template> templates = TemplateSet.loadTemplateSet().getTemplates();
        return templates;
    }

    @RequestMapping(value = "/api/template/save", method = RequestMethod.POST)
    List<TemplateSet.Template> saveTemplate(@RequestBody String[] columns) throws IOException {
        List<TemplateSet.Template> templates = TemplateSet.addTemplateAndSave(columns);
        return templates;
    }

    @RequestMapping(value = "/api/template/delete", method = RequestMethod.GET)
    List<TemplateSet.Template> removeTemplate(@RequestParam("templateId") Long templateId){
        List<TemplateSet.Template> templates = TemplateSet.removeTemplateAndSave(templateId);
        return templates;
    }


    @RequestMapping("/api/csv/download")
    ResponseEntity<byte[]> downloadReport(@RequestBody DownloadReportWraper wraper) throws IOException {
        ReportGenerator generator = new ReportGenerator();
        File report = generator.generate(wraper.getObjects(), wraper.getSelectedColumns());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("text/plain"));
        String filename = "report.csv";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        byte[] csvBytes = Files.readAllBytes(Paths.get(report.getAbsolutePath()));
        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(csvBytes, headers, HttpStatus.OK);
        return response;

    }

    @RequestMapping("/api/csv")
    String[] getCsvList() {
        File directory = new File("CSV_HOLDER");
        if (!directory.exists()) {
            directory.mkdir();
        }
        File[] csvFiles = Utils.getCsvFiles(directory.getAbsolutePath());
        String[] fileNames = new String[csvFiles.length];
        for (int i = 0; i < csvFiles.length; i++) {
            fileNames[i] = csvFiles[i].getName();
        }
        return fileNames;
    }




}
