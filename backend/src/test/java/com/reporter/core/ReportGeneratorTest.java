package com.reporter.core;

import com.reporter.core.DAO.Impl.ModelObjectDaoImpl;
import com.reporter.core.DAO.ModelObjectDao;
import org.junit.Ignore;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Дмитрий on 01.04.2017
 */
public class ReportGeneratorTest {
    ModelObjectDao dao = new ModelObjectDaoImpl("sample.csv");

    public ReportGeneratorTest() throws FileNotFoundException {
    }

    @Ignore
    @Test
    public void generate() throws Exception {
        String[] columns = new String[]{"platform", "vodPlatformName", "dealCode", "masterDealCode", "ibmsControllingChannel", "affiliate", "automatedCatchup", "dealType", "ibmsYear", "number", "mpxProgrammeName", "ibmsSeriesName", "ibmsProgrammeName", "ibmsSeasonNumber", "mpxSeasonNumber", "ibmsEpisodeNo", "mpxEpisodeNo", "mediaID", "titleID", "listingID", "Certification", "ibmsOfferStatus", "mpxProductAdded", "mpxProductUpd", "mpxProductApproved", "mpxProductState", "mpxProductOfferStatus", "productSubscriptions", "ibmsSubscriptionCode", "mpxSubscription", "ibmsOfferHidden", "ibmsArchive", "lpsd", "lped", "ibmsOfferStart", "ibmsOfferEnd", "mpxOfferStart", "mpxOfferEnd", "ibmsOfferStartTime", "ibmsOfferEndTime", "mpxOfferStartTime", "mpxOfferEndTime", "mpxMediaAdded", "mpxMediaUpd", "MediaGCIngestStatus", "ibmsDuration", "mpxDuration", "ibmsTXStatus", "ibmsQCStatus", "ibmsQCStatusUpd", "ibmsQCPassedBy", "mpxProgramAdded", "mpxProgramUpd", "mpxGracenoteExemptStatus", "ibmsGracenoteExemptStatus", "GNStatus", "gracenoteID", "ciscoVAMStatus", "ciscoVAMStatusMessage", "senttoMilano", "skyStatusUp", "acquisitions", "channelCoordinators", "epScheduling", "ottMediaDelivery"};
        List list = dao.find();
        ReportGenerator reportGenerator = new ReportGenerator();
        reportGenerator.generate(list,columns);
    }

}