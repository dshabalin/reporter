package com.reporter.core.model;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by Дмитрий on 05.04.2017
 */
public class TemplateSetTest {
    TemplateSet TEMPLATE_SET_OLD_STATE = TemplateSet.loadTemplateSet();

    @Test
    public void addTemplate() throws Exception {
        String[] columns = {"column1", "column2"};
        List<TemplateSet.Template> templates = TemplateSet.addTemplateAndSave(columns);
        Assert.assertArrayEquals(
                new TemplateSet.Template(columns).columns,
                templates.get(templates.size()-1).columns
        );
        TemplateSet.serialize(TEMPLATE_SET_OLD_STATE);

    }

    @Test
    public void removeTemplate(){
        String[] columns = {"column1", "column2"};
        List<TemplateSet.Template> templates = TemplateSet.addTemplateAndSave(columns);


        List<TemplateSet.Template> templatesRemoved = TemplateSet.removeTemplateAndSave(templates.get(templates.size() - 1).id);

        Assert.assertEquals(TEMPLATE_SET_OLD_STATE.getTemplates(), templatesRemoved);

        TemplateSet.serialize(TEMPLATE_SET_OLD_STATE);

    }





}