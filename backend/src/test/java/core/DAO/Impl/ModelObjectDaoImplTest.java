package core.DAO.Impl;

import com.reporter.core.DAO.Impl.ModelObjectDaoImpl;
import com.reporter.core.DAO.ModelObjectDao;
import com.reporter.core.model.ModelObject;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;

/**
 * Created by Дмитрий on 29.03.2017
 */
public class ModelObjectDaoImplTest {
    private final String CSV = ("sample.csv");
    ModelObjectDao dao = new ModelObjectDaoImpl(CSV);

    public ModelObjectDaoImplTest() throws FileNotFoundException {
    }

    @Test
    public void getAll() {
        List<ModelObject> modelObjects = dao.find();
        Assert.assertNotNull(modelObjects);
        Assert.assertEquals(218, modelObjects.size());
        Assert.assertEquals("NEO1", modelObjects.get(0).platform);
        Assert.assertEquals("NEON", modelObjects.get(1).platform);

        Date expectedDate = new LocalDate(2016, 4, 1).toDate();
        Assert.assertEquals(expectedDate, modelObjects.get(0).lpsd);
        expectedDate = new LocalDate(2018, 3, 31).toDate();
        Assert.assertEquals(expectedDate, modelObjects.get(0).lped);

    }


}